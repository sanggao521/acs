package storage

import (
	"sync"

	"gopkg.in/mgo.v2"
)

// ErrDataNotFound the in the collection could not be found.
var ErrDataNotFound = mgo.ErrNotFound

var mgoSessions = map[string]*mgo.Session{}
var mgoSessionLock = new(sync.RWMutex)

// GetConn retrieves the mongodb connection session instance.
func GetConn(dsn string) (ses *mgo.Session, err error) {
	var created bool
	mgoSessionLock.Lock()
	defer mgoSessionLock.Unlock()
	defer func() {
		if err == nil && ses != nil {
			ses = ses.Clone()
		}
	}()
	if ses, created = mgoSessions[dsn]; created {
		return
	}
	ses, err = mgo.Dial(dsn)
	if err == nil {
		ses.SetMode(mgo.Monotonic, true)
		mgoSessions[dsn] = ses
	}
	return
}
