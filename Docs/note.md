

## 系统主要有这三部分

1. comet 长连接
2. dispatcher 
3. management 管理界面 


## 安装中碰到的问题解决

1. 需要go get 缺失的包，暂时不支持godeps 直接拉取
2. 有个golang的包需要翻墙，如果不能翻墙需要从本地copy到需要的目录中  golang.org/x/crypto/blowfish
3. go rpc 的问题， 这个是一个包，所以需要用git clone 的方式放到和项目同一层结构中
4. management 需要安装etcd, mongodb, mysql,
5. 修改management/conf/config.toml  mysql_db 的配置
6. mongodb 需要创建两个集合
```
use test

db.createCollection("task_contents")

db.createCollection("user_tasks")


```
7. 创建nsqlookupd 的topic upv_topic

```
curl -d 'hello world 1' 'http://127.0.0.1:4151/put?topic=upv_topic'
```

8. 修改 /comet/cmd/config.toml [storage_mgo]的配置, 同事需要在mongodb中创建数据库

event_broker_endpoints 改为自己的

```
use acs_dev

```

9. management/views/index.html gotoLoginPage() 修改返回时跳转的页面 


10. etcd 配到问题 

```
Failed to get all nodes info: client: etcd cluster is unavailable or misconfigured
```

11. dispatcher/cmd/conf/config.toml  event_broker_endpoints = ["http://127.0.0.1:2379"] 配置成需要的



线上部署

1. 首先在ops中配置自己的公钥，然后开通权限使用ssh 登录目标服务器
2. 创建 /home/jm/acs 目录结构，将编译的项目放置到当前目录中， 并创建acs 用户  mkdir -p /home/jm/acs
3. 使用sudo -u acs ./cmd 运行对应的程序
4. scp 上传文件和配置到对应文件夹
5. etcd 的设置:sudo nohup ./etcd --listen-client-urls 'http://0.0.0.0:2379' --advertise-client-urls 'http://0.0.0.0:2379' >/tmp/etcd.log&  
6. mongodb config 文件中 bind_ip=0.0.0.0  改成这个
7. 线上mongo所在位置/usr/bin/mongo
8. /usr/local/acs/management/ 放界面编译文件
9. /usr/local/ 下放置nsq , etcd 等





sudo -u acs ./dispatcher


sudo -u acs ./comet -c config.toml


sudo  nohup ./comet -c config.toml &

sudo -u acs  nohup ./comet -c config.toml &


sudo nohup ./comet -c config.toml &
sudo nohup ./comet -c config-stag.toml &


线上访问后台地址

acsman.int.jumei.com




碰到的问题


1. coment/config.toml    client_handshake_timeout 改成1000 (1s)

外网IP 配置成反向代理ip
wnet_address = 123.59.226.50


10.17.44.15  .这台服务器装comet和dispatcher
10.17.46.43 ，这台装management ，mysql，mongo。nsq，etcd


netstat -anp | grep comet | grep -c ES  //cd
ps -ef | grep comet


http://123.59.226.60:7111/debug/pprof/  查看显示pprof


curl 'http://10.17.46.43:4151/stats?format=json' 查看nsq


//dispather 反向代理
dispatcher-acs.jumei.com

curl 'http://127.0.0.1:4151/stats' //查看nsq节点情况





//userInfo 相关字段解释
appver：就是客户端版本号
bundleid：就是表示当前客户端的包名，比如：com.jm.android之类
system：当前用的系统，比如：ios
osversion：当前用的系统的版本号，比如：10.2
brand：手机厂商名，比如：apple
model：手机型号，比如：iphone
uid：用户名（登录后）
uuid：现在填写的是，设备唯一标示

剩下基本，是以前的时候，热修复以及schema的一些信息，跟过滤无关，不用管了






patch 发送完成会发Patchfin 信号,如果没有就表示没发送成功
event 是客户端发fin 如果没有就表示不成功











