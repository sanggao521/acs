package dispatcher

var (
	Conf       *Config
	configFile string
)

type Config struct {
	//	HttpBind             string   `toml:"dispatcher_server_http_bind"`
	EventBrokerEndpoints []string `toml:"event_broker_endpoints"`
	NodesMonitorDir      string   `toml:"nodes_monitor_dir"`
	EventBrokerAuthinfo  string   `toml:"event_broker_authinfo"`
}

// func init() {
// 	flag.StringVar(&configFile, "conf", "./dispatcher/config.toml", " set config file path")
// 	flag.Parse()
// 	Conf = new(Config)
// 	if _, err := toml.DecodeFile(configFile, Conf); err != nil {
// 		panic(err)
// 	}
// }
