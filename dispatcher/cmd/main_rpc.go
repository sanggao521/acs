package main

import (
	"acs/dispatcher"
	"flag"
	"fmt"
	"os"
	"rpc-go/server/config"
	"rpc-go/server/service"
	"rpc-go/server/service/register"
	"rpc-go/server/transport"
)

func main2() {
	//载入配置文件。默认地址在conf/config.toml
	var configFile string
	flag.StringVar(&configFile, "c", "conf/config.toml", " set config file path")
	flag.Parse()
	conf, err := config.LoadConfig(configFile)
	if err != nil {
		config.Logger.Error("load Config Error:", err.Error())
		os.Exit(1)
	}
	defer config.Logger.Flush()
	monitor, err = dispatcher.NewMonitor(dispatcher.Conf.EventBrokerEndpoints, dispatcher.Conf.EventBrokerAuthinfo, dispatcher.Conf.NodesMonitorDir)
	if err != nil {
		panic(err)
	}
	go monitor.Start()
	srvs := service.NewService(conf)
	register.RegisterHandler("acsdispatcher.serveraddr", serveraddrHandler)
	go srvs.Run()
	signalChan := dispatcher.InitSignal()
	dispatcher.HandleSignal(signalChan)
}

func serveraddrHandler2(conn *transport.JumeiConn, request interface{}) (response string, err error) {
	node := monitor.GetLastSuggestedNodeAddr()
	status := 0
	if node == "" {
		status = 1
		node = "no node to use."
	}
	response = fmt.Sprintf(`{"s": %v, "m":"%v"}`, status, node)
	config.Logger.Infof("acsdispatcher.serveraddr : %s", response)
	return
}
