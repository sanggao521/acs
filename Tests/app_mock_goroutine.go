// 模拟App连接和通讯
package main

import (
	"acs/client"
	"acs/comet/config"
	"acs/comet/proto"
	"acs/pbmodel"
	"acs/taskprocess"
	"errors"
	"fmt"

	//log "github.com/cihub/seelog"
	pb "github.com/golang/protobuf/proto"
	//flags "github.com/jessevdk/go-flags"
)

func main() {
//	logger, err := log.LoggerFromConfigAsString(`<seelog>
//    <outputs formatid="common">
//        <console />
//    </outputs>
//    <formats>
//    <format id="common" format="[%Level]%Date(2006/01/02 15:04:05.999)[%File:%Line] %Msg%n"/>
//</formats>
//</seelog>
//	`)
//	if err != nil {
//		panic(err)
//	}
//	err = log.ReplaceLogger(logger)

	hungCh := make(chan int, 1)

	uids := []int{111, 112, 113, 114, 115, 116}
	concurrent := 60000
	//log.Infof("Start tests with %v mockup clients.", concurrent)
	for ; concurrent > 0; concurrent-- {
		uid := uids[(concurrent-1)%len(uids)]
		c, err := client.NewClient(client.ClientConfig{
			Addr:           "10.1.9.200:8082",
			HeartBeatTtl:   240000,
			Logger:         nil,
			DataEncryptIV:  config.Conf.DataEncryptIV,
			DataEncryptKey: config.Conf.DataEncryptKey,
			HandshakeMessage: client.HandshakeMessage{
				Cmd: proto.CmdRegister,
				ReqMsg: &pbmodel.RegisterInfo{
					Appver:    pb.String("3.0.1"),
					BundleID:  pb.String("com.jumei.live.android"),
					System:    pb.Int32(int32(taskprocess.PlatformAndroid)),
					OSVersion: pb.String("9.3.1"),
					Brand:     pb.String("Huawei"),
					Model:     pb.String("MateS"),
					UID:       pb.String(fmt.Sprintf("%v", uid)),
					UUID:      pb.String("X-xx"),
					LPID:      pb.String("patch-3.0.2-001"),
					LSID:      pb.String("3.0.1-211"),
				},
				Resp: &pbmodel.RegisterInfoResp{},
			},
		})
		fmt.Println(err)
		if err != nil {
			//log.Warnf("%v", err)
			continue
		}
		patchCmdData := new(TestPatchMessage)
		patchCmdRespData := new(pbmodel.PatchfinResp)
		c.RegisterRequestHandler(patchHandler1, proto.CmdPatch, patchCmdData, patchCmdRespData)

	}
	<-hungCh
}

type TestPatchMessage struct {
	pbmodel.Patch
}

func (_ *TestPatchMessage) New() pb.Message {
	return new(pbmodel.Patch)
}
func patchHandler1(patchInfoI pb.Message) (resp pb.Message, err error) {
	patchInfo, ok := patchInfoI.(*pbmodel.Patch)
	if !ok {
		err = errors.New("type error: patch message should be *pbmodel.Patch")
	}
	//log.Debugf("Got ACS message: %+v", *patchInfo)
	fmt.Println(patchInfo, ok)
	patchResp := new(pbmodel.PatchResp)
	patchResp.Msg = pb.String("ok")
	resp = patchResp
	return
}
