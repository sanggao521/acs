// 模拟App连接和通讯
package main

import (
	"acs/client"
	"acs/comet/config"
	"acs/comet/proto"
	"acs/pbmodel"
	"acs/taskprocess"
	"errors"
	"fmt"

	log "github.com/cihub/seelog"
	pb "github.com/golang/protobuf/proto"
	flags "github.com/jessevdk/go-flags"
)

func main() {
	var args struct {
		Concurrency uint16 `short:"n" default:"3" required:"yes" description:"模拟客户端数量"`
	}
	argParser := flags.NewNamedParser("app mock", flags.Default|flags.IgnoreUnknown)
	argParser.AddGroup("Mock parameters", "", &args)
	_, err := argParser.Parse()
	if err != nil {
		panic(err)
	}
	logger, err := log.LoggerFromConfigAsString(`<seelog>
    <outputs formatid="common">
        <console />
    </outputs>
    <formats>
    <format id="common" format="[%Level]%Date(2006/01/02 15:04:05.999)[%File:%Line] %Msg%n"/>
</formats>
</seelog>
	`)
	if err != nil {
		panic(err)
	}
	err = log.ReplaceLogger(logger)

	hungCh := make(chan int, 1)


	var (
		appver  = "3.0.1"
		bundleID = "com.jumei.live.android"
		system = int32(taskprocess.PlatformAndroid)
		oSVersion = "9.3.1"
		brand = "Huawei"
		model = "MateS"

		uUID = "X-xx"
		lPID = "event-3.0.2-001"
		lSID = "3.0.1-211"

	)

	uids := []int{111, 112, 113, 114, 115, 116}
	concurrent := int(args.Concurrency)
	log.Infof("Start tests with %v mockup clients.", concurrent)
	for ; concurrent > 0; concurrent-- {
		uid := uids[(concurrent-1)%len(uids)]
		uidd := fmt.Sprintf("%v", uid)
		c, err := client.NewClient(client.ClientConfig{
			Addr:           "123.59.226.60:8082",
			HeartBeatTtl:   5000,
			Logger:         logger,
			DataEncryptIV:  config.Conf.DataEncryptIV,
			DataEncryptKey: config.Conf.DataEncryptKey,
			HandshakeMessage: client.HandshakeMessage{
				Cmd: proto.CmdRegister,
				ReqMsg: &pbmodel.RegisterInfo{
					Appver:    &appver,
					BundleID:  &bundleID,
					System:    &system,
					OSVersion: &oSVersion,
					Brand:     &brand,
					Model:     &model,
					UID:       &uidd,
					UUID:      &uUID,
					LPID:      &lPID,
					LSID:      &lSID,
				},
				Resp: &pbmodel.RegisterInfoResp{},
			},
		})
		if err != nil {
			log.Warnf("%v", err)
			continue
		}
		patchCmdData := new(TestPatchMessage)
		patchCmdRespData := new(pbmodel.PatchfinResp)
		c.RegisterRequestHandler(eventHandler1, proto.CmdPatch, patchCmdData, patchCmdRespData)

	}
	<-hungCh
}

type TestPatchMessage struct {
	pbmodel.Patch
}

func (_ *TestPatchMessage) New() pb.Message {
	return new(pbmodel.Patch)
}

func eventHandler1(patchInfoI pb.Message) (resp pb.Message, err error) {
	patchInfo, ok := patchInfoI.(*pbmodel.Patch)
	if !ok {
		err = errors.New("type error: patch message should be *pbmodel.Patch")
	}
	log.Debugf("Got ACS message: %+v", *patchInfo)
	patchResp := new(pbmodel.PatchResp)
	var oo string = "ok"
	patchResp.Msg = &oo
	resp = patchResp
	return
}

