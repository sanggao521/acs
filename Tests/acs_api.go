package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"strconv"
)

type person struct {
	Name string
	Age  int
}

func main() {
	var personlist []person
	for i := 0; i < 10; i++ {
		var thisPerson person
		thisPerson.Name = "name" + strconv.Itoa(i)
		thisPerson.Age = i
		personlist = append(personlist, thisPerson)
	}

	var targetPersonlist []person

	for _, item := range personlist {
		var targetperson person
		err := deepCopy(&targetperson, &item)
		fmt.Println(err, targetperson.Name, "----------", targetperson.Age)
		targetPersonlist = append(targetPersonlist, targetperson)
	}
}

func deepCopy(dst, src interface{}) error {
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(src); err != nil {
		return err
	}
	return gob.NewDecoder(bytes.NewBuffer(buf.Bytes())).Decode(dst)
}
func myCopy(dst, src interface{}) error {

	return nil
}
