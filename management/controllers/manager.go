package controllers

import (
	"acs/management/models"
	"net/http"

	"github.com/chaosue/echo-sessions"
	"github.com/labstack/echo"
)

//获取当前登录的用户
func GetCurrentManagerHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)
	currentPerson := sessions.Default(c).Get(models.SESSION_MANAGER)
	c.JSON(http.StatusOK, currentPerson)
	return
}

//登出
func ManagerLogoutHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)
	session := sessions.Default(c)
	session.Delete(models.SESSION_MANAGER)
	session.Save()
	c.Redirect(http.StatusTemporaryRedirect, models.VIEW_LOGIN_URL)
	return
}
