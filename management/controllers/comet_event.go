package controllers

import (
	"rpc-go/client/transport"

	"github.com/labstack/echo"
)

//推送长连接测试消息
func PushCometEvent(c echo.Context) (err error) {
	var jumeiconn transport.JumeiConn
	var uid string
	jumeiconn.Call("acs", "pushEvent", uid, false)

	return nil
}
