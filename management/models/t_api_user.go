package models

import (
	"errors"
	"fmt"
	"strings"

	"github.com/astaxie/beego/orm"
)

type TApiUser struct {
	Id          int64  `orm:"column(id);auto" description:""  form:"id"  json:"id"`
	Token       string `orm:"column(token);size(45);null" description:"token"  form:"token"  json:"token"`
	Description string `orm:"column(description);size(45);null" description:"API和业务描述"  form:"description"  json:"description"`
	UserName    string `orm:"column(user_name);size(45);null" description:"请求时候所带的用户名"  form:"user_name"  json:"user_name"`
	Password    string `orm:"column(password);size(45);null" description:"密码"  form:"password"  json:"password"`
	Enable      int8   `orm:"column(enable);null" description:"1 为可用，0 为删除。默认为1"  form:"enable"  json:"enable"`
}

func (t *TApiUser) TableName() string {
	return "t_api_user"
}
func (t *TApiUser) TableComment() string {
	return "api 用户的表"
}

func init() {
	orm.RegisterModel(new(TApiUser))
}

// AddTApiUser insert a new TApiUser into database and returns
// last inserted Id on success.
func AddTApiUser(m *TApiUser) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetTApiUserById retrieves TApiUser by Id. Returns error if
// Id doesn't exist
func GetTApiUserById(id int64) (v *TApiUser, err error) {
	o := orm.NewOrm()
	v = &TApiUser{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllTApiUser retrieves all TApiUser matches certain condition. Returns empty list if
// no records exist
func GetAllTApiUser(query map[string]interface{}, fields []string, sortby []string, order []string,
	offset int64, limit int64, needCount bool) (ml []TApiUser, totalCount int64, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(TApiUser))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, 0, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, 0, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, 0, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, 0, errors.New("Error: unused 'order' fields")
		}
	}

	var l []TApiUser
	qs = qs.OrderBy(sortFields...)
	if needCount == true {
		if totalCount, err = qs.Count(); err != nil {

			return nil, 0, err
		}
	}
	if _, err = qs.Limit(limit, offset).All(&l); err == nil {

		return l, totalCount, nil
	}
	return nil, totalCount, err
}

// UpdateTApiUser updates TApiUser by Id and returns error if
// the record to be updated doesn't exist
func UpdateTApiUserById(m *TApiUser) (vo *TApiUser, err error) {
	o := orm.NewOrm()
	v := TApiUser{Id: m.Id}
	// ascertain id exists in the database
	//if err = o.Read(&v); err == nil {
	//var num int64
	if _, err = o.Update(m); err == nil {
		return m, nil
		//	fmt.Println("Number of records updated in database:", num)
	}
	//}
	return &v, nil
}

// DeleteTApiUser deletes TApiUser by Id and returns error if
// the record to be deleted doesn't exist
func DeleteTApiUser(id int64) (err error) {
	o := orm.NewOrm()
	v := TApiUser{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&TApiUser{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}
