package models

import (
	"flag"
	"log"

	"github.com/BurntSushi/toml"
	_ "github.com/go-sql-driver/mysql"
)

var (
	Conf       *Config
	configFile string
)

type Config struct {
	JumeiAuth      Jumei_auth      `toml:"jumei_auth"`
	AcsGroup       Acs_group       `toml:"acs_group"`
	NsqUpv         Nsq_upv         `toml:"nsq_upv"`
	JumeiPicsystem Jumei_picsystem `toml:"jumei_picsystem"`
	MysqlDb        Mysql_db        `toml:"mysql_db"`
	AppInfo        App_info        `toml:"app_info"`
	Mongodb        Mongodb         `toml:"mongodb"`
	JumeiApi       JumeiApi        `toml:"jumei_api"`
}
type App_info struct {
	Appname  string `toml:"appname"`
	Httpport string `toml:"httpport"`
	Runmode  string `toml:"runmode"`
	AppKey   string `toml:"app_key"`
}
type Mysql_db struct {
	DbType string `toml:"db_type"`
	DbHost string `toml:"db_host"`
	DbPort string `toml:"db_port"`
	DbUser string `toml:"db_user"`
	DbPass string `toml:"db_pass"`
	DbName string `toml:"db_name"`
}
type Jumei_picsystem struct {
	// 聚美文件上传服务器
	Jumei_picsystem_host string `toml:"jumei_picsystem_host"`
	//用户名
	Jumei_picsystem_name string `toml:"jumei_picsystem_name"`
	//密码
	Jumei_picsystem_pswd string `toml:"jumei_picsystem_pswd"`
	//根目录
	Jumei_picsystem_path string `toml:"jumei_picsystem_path"`

	Jumei_picsystem_CDN string `toml:"jumei_picsystem_CDN"`
}
type Nsq_upv struct {
	//业务端发过来的测用户的app 版本在nsq里的topic名称
	Nsq_user_app_version_topic string `toml:"nsq_user_app_version_topic"`
	//upv 的nsq服务器ip
	Nsq_upv_server_ip string `toml:"nsq_upv_server_ip"`
	//upv 的nsq服务器port
	Nsq_upv_server_port string `toml:"nsq_upv_server_port"`
	//用来匹配推送用户id的正则表达式
	Nsq_upv_uid_regexp string `toml:"nsq_upv_uid_regexp"`
}

type Acs_group struct {
	//管理员组
	Acs_manage_group string `toml:"acs_manage_group"`
	//普通用户组
	Acs_user_group string `toml:"acs_user_group"`
}
type Jumei_auth struct {
	//jumei auth url
	JumeiAuthUrl      string `toml:"jumei_auth_url"`
	JumeiAuthBusiness string `toml:"jumei_auth_business"`
	JumeiAauthName    string `toml:"jumei_auth_name"`
	JumeiAuthPswd     string `toml:"jumei_auth_pswd"`
	JumeIAuthAcs      string `toml:"jumei_auth_acs"`
	JumeiAuthApiLogin string `toml:"jumei_auth_api_login"`
	JumeIAuthLoginUrl string `toml:"jumei_auth_login_url"`
}
type Mongodb struct {
	MongoServerIP                 string `toml:"mongo_server_ip"`
	MongoServerUser               string `toml:"mongo_server_user"`
	MongoServerPswd               string `toml:"mongo_server_pswd"`
	MongoUpvDbname                string `toml:"mongo_upv_dbname"`
	MongoUpvTaskcontentCollection string `toml:"mongo_upv_taskcontent_collection"`
	MongoUpvUsertaskCollection    string `toml:"mongo_upv_usertask_collection"`
}

type JumeiApiItem struct {
	Id     int    `toml:"id" json:"id"`
	Url    string `toml:"url" json:"url"`
	Desc   string `toml:"desc" json:"desc"`
	Method string `toml:"method" json:"method"`
}
type JumeiApi struct {
	JumeiApiItemList []JumeiApiItem `toml:"jumei_api_items"`
}

func LoadConfig() (err error) {
	flag.StringVar(&configFile, "c", "conf/config.toml", " set config file path")
	flag.Parse()
	Conf = new(Config)
	if _, err = toml.DecodeFile(configFile, Conf); err != nil {
		log.Panicln("failed to load conf/config.toml" + err.Error())
		return
	}
	return nil
}
