package models

import (
	"log"
	"os"

	"github.com/astaxie/beego/orm"
)

func InitMysql() (err error) {
	connetDBString := Conf.MysqlDb.DbUser + ":" + Conf.MysqlDb.DbPass + "@tcp(" + Conf.MysqlDb.DbHost + ":" + Conf.MysqlDb.DbPort + ")/" + Conf.MysqlDb.DbName + "?charset=utf8&loc=Local"
	err = orm.RegisterDataBase("default", Conf.MysqlDb.DbType, connetDBString)
	if err != nil {
		log.Panicln("failed to connect to mysql server:" + err.Error())
		os.Exit(1)
		return
	}
	if Conf.AppInfo.Runmode == "dev" {
		orm.Debug = true
	}
	return nil
}
