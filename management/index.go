package main

import (
	"acs/management/controllers"
	"encoding/gob"
	"net/http"
	"syscall"

	"acs/management/models"
	"log"
	"os"

	"github.com/chaosue/echo-sessions"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/sunreaver/grace/gracehttp"
)

func init() {
	//初始化相关服务器的连接
	if err := models.LoadConfig(); err != nil {
		os.Exit(1)
	}
	if err := models.InitMgo(); err != nil {
		os.Exit(1)
	}
	if err := models.InitMysql(); err != nil {
		os.Exit(1)
	}
	if err := models.InitNsqServer(); err != nil {
		os.Exit(1)
	}
}

func main() {
	e := echo.New()
	middleware.DefaultStaticConfig.Index = "default.html"
	store := sessions.NewCookieStore([]byte("acsmanagement"))
	e.Use(sessions.Sessions("echosession", store))
	if models.Conf.AppInfo.Runmode == "dev" {
		models.Conf.AppInfo.AppKey = "300834066f2211e68d73842b2b738d12"
		models.Conf.AppInfo.Appname = "acs_dev"
		e.Debug = true
		e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
			Format: "method=${method}, uri=${uri}, status=${status}\n",
		}))
	}
	gob.Register(models.User{})
	models.LoadAllPrivilege()
	models.LoadAllApiUrl()
	models.LoadAllUser()
	models.LoadAllApiUser()
	models.LoadAllApiUserUrl()
	e.Use(middleware.Recover())
	e.Use(middleware.Static("./views"))
	//----------------管理员-----------------
	e.GET("/", controllers.UserIndexHandler)
	//	e.GET("/login", controllers.UserLoginHandler)
	e.GET("/loginSuccess.html", controllers.ManagerLoginSuccessHandler)
	// e.GET("/get_currentmanager", controllers.GetCurrentManagerHandler)
	// e.GET("/manager_logout", controllers.ManagerLogoutHandler)
	//----------------用户-----------------
	//e.Post("/add_user/all/:all", controllers.AddUserHandler)
	//	e.GET("/get_user/:id", controllers.GetUserHandler)
	e.GET("/get_currentuser", controllers.GetCurrentUserHandler)
	e.GET("/user_logout", controllers.UserLogoutHandler)
	//e.Post("/update_user/:id", controllers.EditUserHandler)
	//e.GET("/delete_user/:id/enable/:enable", controllers.KillUserHandler)
	e.GET("/list_user/:group", controllers.ListUserHandler)
	//----------------App-----------------
	e.POST("/add_app/all/:all", controllers.AddAppHandler)
	e.GET("/get_app/:id", controllers.GetAppandler)
	//	e.POST("/update_app/:id", controllers.EditAppHandler)
	//	e.GET("/delete_app/:id/enable/:enable", controllers.KillAppHandler)
	e.GET("/list_app/:page/step/:step/all/:all", controllers.ListAppHandler)
	e.GET("/list_basic_info/:page/step/:step/type/:type", controllers.ListBasicInfoHandler)
	e.POST("/add_basic_info", controllers.AddBasicInfoHandler)
	e.DELETE("/delete_basic_info/:id", controllers.DeleteBasicInfoHandler)
	e.GET("/list_basic_info_all/system/:system", controllers.ListBasicInfoByParamsHandler)
	//----------------App patch -----------------
	e.POST("/add_patch", controllers.AddPatchHandler)
	//e.GET("/get_patch/:id", controllers.GetPatchandler)
	//	e.POST("/update_patch/:id", controllers.EditPatchHandler)
	e.GET("/delete_patch/:id/enable/:enable", controllers.KillPatchHandler)
	//e.GET("/list_patch/:page/step/:step", controllers.ListPatchHandler)
	///page/"+ page + "/step/" + step + "/all/
	e.GET("/list_patch_by_app/:appid/page/:page/step/:step/all/:all", controllers.ListPatchByAppHandler)
	e.GET("/get_patch_speed", controllers.GetPatchSpeedByAppHandler)
	//----------------Log  -----------------
	e.GET("/get_log/:id", controllers.GetLogHandler)
	e.POST("/list_log/:page/step/:step", controllers.ListLogHandler)
	//----------------Privilege  -----------------
	e.GET("/list_target/:target/user/:username", controllers.GetTargetListHandler)
	e.POST("/user_privilege/:username", controllers.SaveUserPrivilegeHandler)
	//----------------api user  -----------------
	e.POST("/add_api_user", controllers.AddAPIUSERHandler)
	e.GET("/list_api_user/:page/step/:step", controllers.ListAPIUSERHandler)
	//----------------api url  -----------------
	e.POST("/add_api_url", controllers.AddApiUrlHandler)
	e.GET("/list_api_url/:page/step/:step", controllers.ListApiUrlHandler)
	//----------------api user url  -----------------
	e.POST("/api_user_url/user/:api_user_token", controllers.SaveApiUserUrlByUserHandler)
	e.POST("/api_user_url/:api_url_id", controllers.SaveApiUserUrlByURLHandler)
	e.GET("/list_api_user_url/:api_url_id", controllers.GetApiUserListByAPIHandler)
	//-----------------upload -------------------
	//上传app补丁包文件 到cdn
	e.POST("/uploadPatchfile", controllers.PatchUpload)
	//上传需要升级的用户id列表文件。
	e.POST("/upload_upv_file", controllers.UploadUpvFile)
	//-----------------对外提供的 api 列表 -------------------
	e.GET("/api/list_app/:page/step/:step/all/:all", controllers.OutApiListAPPHandler)
	e.GET("/api/list_patch_by_app/:appid/page/:page/step/:step/all/:all", controllers.OutApiListPatchByAPPHandler)
	e.GET("/get_access_api_speed/:all", controllers.GetAccessAPISpeedHandler)

	//--------------对业务部门开放的api 列表------------------------
	e.POST("/send_user_patch_version", controllers.PostUserPatchVersion)
	//	e.POST("/push_upv", controllers.PushUPV)

	e.POST("/upv_task/task_content/add", controllers.ADDUPVTaskContent)
	e.GET("/upv_task/task_content/list/:page/:step", controllers.ListUPVTaskContent)
	e.GET("/upv_task/task_content/item/:id", controllers.GetUPVTaskContent)
	e.PUT("/upv_task/task_content/:id", controllers.UpdateUPVTaskContent)
	e.DELETE("/upv_task/task_content/:id", controllers.DeleteUPVTaskContent)
	e.PUT("/upv_task/task_content/list", controllers.DeleteUPVTaskContentList)

	e.POST("/upv_task/user_task/add", controllers.ADDUPVUserTask)
	e.GET("/upv_task/user_task/list/:page/:step", controllers.ListUPVUserTask)
	e.GET("/upv_task/user_task/item/:id", controllers.GetUPVUserTask)
	e.PUT("/upv_task/user_task/:id", controllers.UpdateUPVUserTask)
	e.DELETE("/upv_task/user_task/:id", controllers.DeleteUPVUserTask)
	e.PUT("/upv_task/user_task/list", controllers.DeleteUPVUserTaskList)

	//上传需要升级的用户id列表文件。
	e.POST("/upload_pushuids_file", controllers.UploadUpvEventFile)

	log.Println("running at port", models.Conf.AppInfo.Httpport)
	// e.Run(standard.New(models.Conf.AppInfo.Httpport))

	v2 := e.Group("/v2")

	v2.POST("/api/push_event", controllers.V2UpvEvent)

	gracehttp.SetLogger(log.New(os.Stdout, "[grace]", 1))
	gracehttp.Serve(syscall.SIGHUP, &http.Server{
		Addr:    models.Conf.AppInfo.Httpport,
		Handler: e,
	})
}

// func notifySignal(s chan os.Signal) {
// 	signal.Notify(s, syscall.SIGINT, syscall.SIGQUIT, os.Kill)
// }
// func listenSignal(s chan os.Signal) {
// 	for {
// 		select {
// 		case <-s:
// 			fmt.Println("--------exiel ")
// 		}

// 	}
// }
