package hook

import "encoding/json"

type errorFormat struct {
	Code   int    `json:"code"`
	Detail string `json:"detail"`
}

func makeErrorFormat(data string) (*errorFormat, error) {
	var ef errorFormat
	e := json.Unmarshal([]byte(data), &ef)
	return &ef, e
}
