#!/bin/bash

if [[ $1 == "" ]]; then
	echo "需要输入proto文件名"
	exit 1
fi

IN_PATH=./
OUT_PATH=${IN_PATH}

protoc --version

protoc --proto_path=${IN_PATH}:. --micro_out=${OUT_PATH} --go_out=${OUT_PATH} ${IN_PATH}/$1 && sed -i .jmmicrotmp "s/github.com\/micro\/go-micro/gitee.com\/JMArch\/micro/g" ${OUT_PATH}/*.micro.go && rm ${OUT_PATH}/*.jmmicrotmp

