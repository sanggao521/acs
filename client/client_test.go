package client

import (
	"acs/comet/proto"
	encrypt "acs/comet/proto"
	"acs/pbmodel"
	"testing"

	"net/url"

	pb "github.com/golang/protobuf/proto"
)

var client *Client

func init() {
	c, err := NewClient(ClientConfig{
		Addr:         "123.59.226.60:8082",
		HeartBeatTtl: 1000,
		Loglevel:     LogDebug,
		HandshakeMessage: HandshakeMessage{
			Cmd: proto.CmdRegister,
			ReqMsg: &pbmodel.RegisterInfo{
				Appver:    pb.String("3.0.1"),
				BundleID:  pb.String("com.jumei.jm.andr oid"),
				System:    pb.Int(1),
				OSVersion: pb.String("9.3.1"),
				Brand:     pb.String("Huawei"),
				Model:     pb.String("MateS"),
				UID:       pb.String("0"),
				UUID:      pb.String("X-xx"),
				LPID:      pb.String("3.0.1-003"),
				LSID:      pb.String("3.0.1-211"),
			},
			Resp: &pbmodel.RegisterInfoResp{},
		},
		DataEncryptIV:  "configiv",
		DataEncryptKey: "数据加密密钥",
	})
	if err != nil {
		//(err)
	}
	client = c
}

func TestPathFin(t *testing.T) {
	_, _, err := client.SendMessage(proto.CmdPathfin, &pbmodel.Patchfin{
		LPID: pb.String("3.0.1-004"),
	},
		&pbmodel.PatchfinResp{},
	)
	if err != nil {
		t.Error(err)
	}
}

func TestForwardHttp(t *testing.T) {
	resp := &pbmodel.ForwardHttpResp{}
	form := url.Values{
		"d": []string{`{"device":{"aid":"com.jm.android.jumei","platform":"android","cv":"3.818","cs":"xiaomi",
	    "dm":"MI NOTE Pro","ov":"5.1.1","imei":"867694020296838","db":"Xiaomi","dn":"wifi","op":"中国移动",
	    "pm":"f4:8b:32:cc:50:ad","ip":"112.193.202.225","uid":"123456789","site":"cd","dr":"1440*2560","os":"android",
	    "sid":"c33198210cd699122d0cc79e99c5e27e"},"browse":[{"p":"home","t":"1460691067","pa":"pageflag=home_main"}],
	    "event":[{"id":"click_search_bar","t":"1460691070","p":"home"},{"id":"search","t":"1460691074","p":"search_result",
	    "ea":"searchWord=希思黎&resultType=1&resultCnt=99"}]}`},
	}
	/*form := url.Values{
		"d": []string{"123"},
	}*/
	item := pbmodel.KeyValue{
		Key:   pb.String("Content-Type"),
		Value: pb.String("application/x-www-form-urlencoded"),
	}
	m := pbmodel.StringMap{}
	m.Items = make([]*pbmodel.KeyValue, 0)
	m.Items = append(m.Items, &item)
	_, re, err := client.SendMessage(proto.CmdForward, &pbmodel.ForwardHttp{
		Method: pb.String("post"),
		Url:    pb.String(`http://mtr.jumei.com/mon`),
		Body:   []byte(form.Encode()),
		Header: &m,
	}, resp)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("%+v", re)
}

func TestEncrypt(t *testing.T) {
	rawData := []byte{8, 240, 46, 18, 0}
	t.Logf("raw data: %+v", rawData)
	encryptData, err := encrypt.Encrypt(rawData, "数据加密密钥", "configiv")
	if err != nil {
		t.Errorf("encrypt error: %s", err.Error())
	}
	t.Logf("encrypt data: %+v", encryptData)
	//data := []byte{208, 234, 64, 41, 136, 131, 215, 145}
	decryptData, err := encrypt.Decrypt(encryptData, "数据加密密钥", "configiv")
	if err != nil {
		t.Errorf("decrypt error: %s", err.Error())
	}
	t.Logf("decrypt data: %+v", decryptData)
}
